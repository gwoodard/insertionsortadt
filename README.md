# Insertion Sort ADT #

### **About:** ###

Insertion Sort ADT program that allows users to input 5 numbers in an array. The numbers program calls the Insertion Sort algorithm then sorts and display each number in the sorted order.

### **IDE:**
Visual Studio

### **Input:** ###
5 integers

### **Output:** ###
Numbers sorted based on Insertion Sort Algorithm

### **Language:** ###
C++