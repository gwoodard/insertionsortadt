/*
George Woodard
CS 215
10/21/2014
Programming Assignment #8
Insertion Sort Algorithm

*/


#include <iostream>
using namespace std;
typedef int myType;

void insertionSort (myType a[], int n)
{
	for (int unSorted = 1; unSorted < n; ++unSorted)
	{
		myType nextItem = a[unSorted];
		int loc = unSorted;
		for(; (loc > 0) && (a[loc - 1] > nextItem); --loc)
		{
			a[loc] = a[loc - 1];
		}
		a[loc] = nextItem;
	}
}

int main()
{
	const int SIZE = 5;
	myType a[SIZE];
	int n = SIZE;
	int i;

	for (i = 0; i < SIZE; i++)
	{
		//Prompts the user for input of 5 numbers in the array
		cout<<"Enter a whole number: ";
		cin>>a[i];
	}

	//Calls the Insertion Sort Algorithm
	insertionSort(a, SIZE);

	cout<<endl;
	//Displays each of the numbers in the sorted order
	for(i = 0; i < SIZE; i++)
		cout<<a[i]<<endl;

	system("pause");
	return 0;

}